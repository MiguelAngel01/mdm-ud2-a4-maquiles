package dam.android.MAQuiles.u2p4conversor;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

@RequiresApi(api = Build.VERSION_CODES.N)
public class MainActivity extends LogActivity {

    DecimalFormat formato1 = new DecimalFormat("#.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){

        final EditText etPulgada = findViewById(R.id.et_Pulgada);
        final EditText etResultado = findViewById(R.id.et_Resultado);
        final TextView errorText = findViewById(R.id.error);
        Button buttonConvertir = findViewById(R.id.button_Convertir);

        buttonConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Comprueba que unidad de medida se ha introducido
                    if (etPulgada.getText().length() != 0 && etResultado.getText().length() == 0){
                        etResultado.setText(convertirInchToCm(etPulgada.getText().toString()));
                        errorText.setText("");
                    } else if (etPulgada.getText().length() == 0 && etResultado.getText().length() != 0){
                        etPulgada.setText(convertirCmToInch(etResultado.getText().toString()));
                        errorText.setText("");
                    } else {
                        Log.e("LogConversor", "Vacia uno de los campos");
                        errorText.setText("Vacia uno de los campos");
                    }
                } catch (NumeroMenor1Exception e) { //Excepcion propia creada para comprobar que no se insertan numeros menores a cero
                    errorText.setText(e.getMessage());
                    Log.e("LogConversor", e.getMessage());
                }catch (Exception e){
                    Log.e("LogConversor", e.getMessage());
                }
            }
        });

    }

    private String convertirCmToInch(String cm) throws NumeroMenor1Exception {

        if (Integer.parseInt(cm) < 1){
            throw new NumeroMenor1Exception();
        }

        double cmValue = Double.parseDouble(cm) / 2.54; //Convertir cm a pulgadas

        return String.valueOf((formato1.format(cmValue)));

    }

    private String convertirInchToCm(String pulgadas) throws NumeroMenor1Exception {

        if (Integer.parseInt(pulgadas) < 1){
            throw new NumeroMenor1Exception();
        }

        double pulgadaValue = Double.parseDouble(pulgadas) * 2.54; //Convertir pulgadas a cm

        return String.valueOf(formato1.format(pulgadaValue));

    }

}